import React, { Component } from 'react';
import {
  StyleSheet, Image, TouchableHighlight, Modal, TextInput, ActivityIndicator, AsyncStorage
} from 'react-native';
import Feather from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Foundation from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Container, Header, Content, Form, Item, Input, Title, Icon, Label, Button, Text, Thumbnail, Right, Left, Body, View, Card, CardItem, List, ListItem } from 'native-base';
import CardDetails from './CardDetails';
import SideBar from "./SideBar";
import MyOffer from './MyOffer';
import LinearGradient from 'react-native-linear-gradient';
import logoimage from '../images/logoimage.png';
import dumpimage from '../images/dump.jpg';
import styles from '../styles/usercard';
import { Actions } from 'react-native-router-flux';
import { fetchOffer, fetchSearch, fetchAddHistory, fetchResetHistory } from '../redux/actions/OfferActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import OfferList from '../components/OfferList';
import CategoryList from '../components/CategoryList';
import SearchList from '../components/SearchList';
import * as Progress from 'react-native-progress';
import BackgroundGeolocation from 'react-native-mauron85-background-geolocation';
import geolib from 'geolib';
import TimerMixin from 'react-timer-mixin';
const updateLocation = 'update-location:';//this is curren user location
const updateDistance = 'update-location-distance:';//this is update distance for the location background tracking
const updateDistanceCompare = 'update-location-distance-compare:'; //this is log tag for the distance if user are in 30 mile
const setdistance = 28000;//this is vertual distance for the testing!
const settime = 7200000;//this is settime every 2 hours
const timestatus = 'update-location-time:';
// i will show to you how to testing it using console log,so i am running drivie mode for the testing on ios simulator
class Usercard extends Component {

  constructor(props) {
    super(props);
    this.search = this.search.bind(this);
    this.search_all = this.search_all.bind(this);
    this.gotocarddetails = this.gotocarddetails.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this._getLocationTracking = this._getLocationTracking.bind(this);
    this._setPostTimer = this._setPostTimer.bind(this);
    this._compareSetDistance = this._compareSetDistance.bind(this);
    this._setLocation = this._setLocation.bind(this);
    this._sendLocation = this._sendLocation.bind(this);
    search_keyword = '';
    history = '';

    this.state = {
      searckeyword: '',
      fontLoaded: false,
      location:{
        latitude:0,
        longitude:0
      }
    };

  }

  search() {
    this.props.fetchSearch(this.state.searckeyword);
    this.props.fetchAddHistory(this.state.searckeyword);
    this.closeModal();
  }

  search_all() {
    this.props.fetchOffer();
    this.props.fetchResetHistory();
  }

  gotocarddetails() {
    Actions.offerdetails();
  }

  componentWillMount() {
    this.setState({ popupWindow: "0" });
    this.setState({ modalVisible: false });
  }

  openModal() {
    this.setState({ modalVisible: true });
  }

  closeModal = () => {
    this.setState({ modalVisible: false });
  }

  async componentDidMount() {
    try {
      this.setState({ fontLoaded: true });
    } catch (error) {
      console.log(error);
    }
    this.props.fetchOffer();
    this._getLocationTracking();
  }
// geo utility function
_setPostTimer(){
  mixins: [TimerMixin];
  this.interval = setInterval(() => {
    console.log(timestatus,'send-location!');
    this._sendLocation();

  }, settime); //every 2 hours
}
_sendLocation(){
  let location = this.state.location;
}
_compareSetDistance(location){
  let discance = geolib.getDistance({
    latitude: location.latitude,
    longitude: location.longitude
  }, {
    latitude: 37.66423047,
    longitude: -122.46553746
  })
  console.log(updateDistance,discance);
  if(discance<setdistance){
    // alert('you are in home zone!');
    console.log(updateDistanceCompare,'you are in 30 mile range!');
    this._setPostTimer();
  }
}
_setLocation(location){
  this.setState({
    location:{
      latitude:location.latitude,
      longitude:location.longitude
    }
  });
  console.log(updateLocation,this.state.location);
}
_getLocationTracking(){
  BackgroundGeolocation.configure({
    desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
    stationaryRadius: 50,
    distanceFilter: 50,
    notificationTitle: 'Background tracking',
    notificationText: 'enabled',
    debug: false,
    startOnBoot: false,
    stopOnTerminate: false,
    startForeground: true,
    locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
    interval: 10000,
    fastestInterval: 5000,
    activitiesInterval: 10000,
    stopOnStillActivity: false,
    url: 'http://192.168.81.15:3000/location',
    httpHeaders: {
      'X-FOO': 'bar'
    },
    // customize post properties
    postTemplate: {
      lat: '@latitude',
      lon: '@longitude',
      foo: 'bar' // you can also add your own properties
    }
  });

  BackgroundGeolocation.on('location', (location) => {
    // handle your locations here
    // to perform long running operation on iOS
    // you need to create background task
    console.log('update-location:',location);
    this._compareSetDistance(location);
    this._setLocation(location);
    BackgroundGeolocation.startTask(taskKey => {
      // execute long running task
      // eg. ajax post location
      // IMPORTANT: task has to be ended by endTask
      BackgroundGeolocation.endTask(taskKey);
    });
  });

  BackgroundGeolocation.on('stationary', (stationaryLocation) => {
    // console.log('update-location:',location)
    // handle stationary locations here
    // Actions.sendLocation(stationaryLocation);
  });

  BackgroundGeolocation.on('error', (error) => {
    console.log('[ERROR] BackgroundGeolocation error:', error);
  });

  BackgroundGeolocation.on('start', () => {
    console.log('[INFO] BackgroundGeolocation service has been started');
  });

  BackgroundGeolocation.on('stop', () => {
    console.log('[INFO] BackgroundGeolocation service has been stopped');
  });

  BackgroundGeolocation.on('authorization', (status) => {
    console.log('[INFO] BackgroundGeolocation authorization status: ' + status);
    if (status !== BackgroundGeolocation.AUTHORIZED) {
      // we need to set delay or otherwise alert may not be shown
      setTimeout(() =>
        Alert.alert('App requires location tracking permission', 'Would you like to open app settings?', [
          { text: 'Yes', onPress: () => BackgroundGeolocation.showAppSettings() },
          { text: 'No', onPress: () => console.log('No Pressed'), style: 'cancel' }
        ]), 1000);
    }
  });

  BackgroundGeolocation.on('background', () => {
    console.log('[INFO] App is in background');
  });

  BackgroundGeolocation.on('foreground', () => {
    console.log('[INFO] App is in foreground');
  });

  BackgroundGeolocation.checkStatus(status => {
    console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
    console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
    console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);

    // you don't need to check status before start (this is just the example)
    if (!status.isRunning) {
      BackgroundGeolocation.start(); //triggers start on start event
    }
  });
}
  render() {
    if (this.state.fontLoaded) {
      let offers = this.props.randomoffer.offer.filter((offer) => offer.userId === "" || !offer.userId);
      console.log('offerlist:',offers);
      let content = <OfferList offer={offers} />;
      if (this.props.randomoffer.isFetching) {
        content = <Progress.Circle size={30} indeterminate={true} style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', }} />;
      }

      let content_search = <SearchList close={this.closeModal} />;
      if (this.props.randomoffer.isFetching) {
        content_search = <Progress.Circle size={30} indeterminate={true} />;
      }
      return (
        <Container style={styles.container}>
          <Grid>
            <Row style={{ height: 94 }}>
              <Content style={styles.header}>
                <Grid>
                  <Col>
                    <TouchableHighlight style={{ marginTop: 40, marginLeft: 19 }} onPress={() => { Actions.drawerOpen(); }}>
                      <Feather name='menu' size={35} color="#0B9496" />
                    </TouchableHighlight>
                  </Col>
                  <Col>
                    <Image source={logoimage} style={styles.logo} />
                  </Col>
                  <Col >
                    <Button transparent style={{ alignSelf: 'flex-end', marginRight: 19, marginTop: 37 }} onPress={() => this.openModal()}>
                      <Foundation name='filter' size={30} color="#0B9496" />
                    </Button>
                  </Col>
                </Grid>
              </Content>
            </Row>
            <Row>
              <Content style={{ flex: 1, alignSelf: 'flex-start', }}>
                {content}
              </Content>
            </Row>
          </Grid>
          <Modal
            visible={this.state.modalVisible}
            animationType={'fade'}
            onRequestClose={() => this.closeModal()}
          >
            <Grid>
              <Row style={{ height: 74, backgroundColor: '#000000' }}>
                <Grid>
                  <Col>
                    <TouchableHighlight onPress={() => this.setState({ popupWindow: "0" })} style={this.state.popupWindow == "0" ? styles.popcontrolbutton_left : styles.popcontrolbutton_left_deactivate}>
                      <Text style={styles.buttontext_small}>Categories</Text>
                    </TouchableHighlight>
                  </Col>
                  <Col>
                    <TouchableHighlight onPress={() => this.setState({ popupWindow: "1" })} style={this.state.popupWindow == "1" ? styles.popcontrolbutton_right : styles.popcontrolbutton_right_deactive}>
                      <Text style={styles.buttontext_small}>Search</Text>
                    </TouchableHighlight>
                  </Col>
                </Grid>
              </Row>
              <Row style={{ backgroundColor: '#000000' }}>
                {this.state.popupWindow == "0"
                  ? <View>
                    <Text style={styles.categorytitletext}>Categories</Text>
                    <CategoryList close={this.closeModal} />
                  </View>
                  :
                  null
                }
                {this.state.popupWindow == "1"
                  ? <Grid>
                    <Row style={{ height: 80 }}>
                      <Grid>
                        <Col>
                          <TextInput placeholder={'Search'} autoCapitalize={'none'} placeholderTextColor={'#FFFFFF'} style={{ width: 280, alignSelf: 'stretch', marginLeft: 29, color: '#FFFFFF', fontSize: 30, fontFamily: 'Montserrat-SemiBold', marginTop: 30, borderLeftColor: '#000000', borderTopColor: '#000000', borderRightColor: '#000000', borderBottomColor: '#FFFFFF', borderWidth: 1 }}
                            onChangeText={
                              (text) => (
                                this.setState({ searckeyword: text }
                                ))
                            }
                            onSubmitEditing={this.search} />
                        </Col>
                        <Col>
                          <TouchableHighlight onPress={this.search} >
                            <FontAwesome name='search' size={30} color="#FFFFFF" style={{ alignSelf: 'flex-end', marginRight: 30, marginTop: 30 }} />
                          </TouchableHighlight>
                        </Col>
                      </Grid>
                    </Row>
                    <Row style={{ flex: 1, alignSelf: 'center', justifyContent: 'center', }}>
                      {content_search}
                    </Row>
                  </Grid>
                  :
                  null

                }
              </Row>
              <Row style={{ height: 100, backgroundColor: '#000000' }}>
                <Grid>
                  <Col>
                    {this.state.popupWindow === "1" ?
                      <TouchableHighlight onPress={this.search_all} >
                        <Text style={styles.clearbutton}>Clear</Text>
                      </TouchableHighlight>
                      : null}
                  </Col>
                  <Col>
                    <TouchableHighlight onPress={() => this.closeModal()}>
                      <Text style={styles.cancelbutton}>Cancel</Text>
                    </TouchableHighlight>
                  </Col>
                </Grid>
              </Row>
            </Grid>
          </Modal>
        </Container>);
    } else {
      return (<Text>loading..</Text>);
    }
  }
}
function mapStateToProps(state, props) {
  return {
    randomoffer: state.OfferReducer
  }
}
export default connect(mapStateToProps, { fetchOffer, fetchSearch, fetchAddHistory, fetchResetHistory })(Usercard);






